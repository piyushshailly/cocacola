import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { AuthGuardService as AuthGuard } from "~/app/core/services/auth-guard.service";

const routes: Routes = [
    { path: "", redirectTo: "/home", pathMatch: "full" },
    {
        path: "home", loadChildren: "~/app/home/home.module#HomeModule",
        canActivate: [AuthGuard]
    },
    { path: "login", loadChildren: "~/app/login/login.module#LoginModule" },
    {
        path: "settings", loadChildren: "~/app/settings/settings.module#SettingsModule",
        canActivate: [AuthGuard]
    },
    {
        path: "records", loadChildren: "~/app/records/records.module#RecordsModule",
        canActivate: [AuthGuard]
    }
];

@NgModule({
    imports: [NativeScriptRouterModule.forRoot(routes)],
    exports: [NativeScriptRouterModule]
})
export class AppRoutingModule { }
