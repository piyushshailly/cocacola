import { Component, ElementRef, ViewChild, OnDestroy } from "@angular/core";
import { Router } from "@angular/router";
import { alert, prompt } from "tns-core-modules/ui/dialogs";
import { Page } from "tns-core-modules/ui/page";
import { AuthService } from "../core/services/auth.service";
import { Subscription } from "rxjs";

@Component({
    selector: "app-login",
    moduleId: module.id,
    templateUrl: "./login.component.html",
    styleUrls: ["./login.component.css"]
})
export class LoginComponent implements OnDestroy {
    errorMessage: string = "";
    loginSubscription: Subscription;

    @ViewChild("password", {static: false}) password: ElementRef;

    constructor(private page: Page, private router: Router, private authService: AuthService) {
        this.page.actionBarHidden = true;
    }

    submit(username: string, password: string) {
        if (!username || !password) {
            this.alert("Please provide both an email address and password.");

            return;
        }
        this.login(username, password);
    }

    login(username: string, password: string) {
        this.authService.login(username, password).subscribe((data) => {
            this.router.navigate(["home"]);
            this.errorMessage = "";
        }, (err) => {
            this.errorMessage = "Wrong username or password!";
        });
    }

    focusPassword() {
        this.password.nativeElement.focus();
    }

    alert(message: string) {
        return alert({
            title: "Coca-Cola",
            okButtonText: "OK",
            message
        });
    }

    ngOnDestroy() {
        if (this.loginSubscription) {
            this.loginSubscription.unsubscribe();
        }
    }
}
