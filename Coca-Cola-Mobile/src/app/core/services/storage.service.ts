import { Injectable } from "@angular/core";
import * as appSettings from "tns-core-modules/application-settings";

@Injectable({
  providedIn: "root"
})
export class StorageService {

  constructor() {}

  set(key: string, value: string): void {
    appSettings.setString(key, value);
  }

  get(key: string): string {
    return appSettings.getString(key);
  }

  remove(key: string): void {
    appSettings.remove(key);
  }
}
