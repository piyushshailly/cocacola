import { BadRequestException } from '@nestjs/common';

export class CustomerBadRequest extends BadRequestException {}
