import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { RoleDTO } from 'src/models/role/role.dto';

@Injectable()
export class RolesIndividualGuard implements CanActivate {
    constructor(private readonly reflector: Reflector) { }

    canActivate(context: ExecutionContext): boolean {
        const roles = this.reflector.get<RoleDTO[]>('roles', context.getHandler());
        if (!roles) {
            return false;
        }

        const request = context.switchToHttp().getRequest();
        const user = request.user;
        const hasRole = () => user.roles.some(role => roles.includes(role.name));

        return user && user.roles && hasRole();
    }
}
