export enum RedemptionCodeStatus {
    valid = 'valid',
    redeemed = 'redeemed',
    canceled = 'canceled',
    invalid = 'invalid',
    reported = 'reported',
}
