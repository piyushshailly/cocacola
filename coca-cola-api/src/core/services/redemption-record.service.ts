import { RedemptionRecordNotFound } from './../../common/exceptions/redemption-record-not-found';
import { ShowRedemptionRecordDTO } from './../../models/redemption-records/show-redemption-record.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { RedemptionRecord } from '../../data/entities/redemption-record.entity';
import { Repository } from 'typeorm';
import { plainToClass } from 'class-transformer';
import { UserDTO } from 'src/models/user/user.dto';

export class RedemptionRecordService {

    constructor(
        @InjectRepository(RedemptionRecord) private readonly redemptionRecordRepository: Repository<RedemptionRecord>,
    ) { }

    async getAllRedemptionRecords(): Promise<ShowRedemptionRecordDTO[]> {
        // Search for redemption records in repository
        const redemptionRecords = await this.redemptionRecordRepository.find({
            order: {
                id: 'ASC',
            },
            relations: ['redemptionCode', 'updatedByUser', 'customer', 'outlet'],
        });

        // Throw error if no records was found
        if (!redemptionRecords) {
            throw new RedemptionRecordNotFound('No redemption records was found!');
        }

        // Conversion
        return plainToClass(ShowRedemptionRecordDTO, redemptionRecords, { excludeExtraneousValues: true });
    }

    async getRecords(user: UserDTO): Promise<any> {
        // Search for redemption records in repository
        const redemptionRecords: any = await this.redemptionRecordRepository.find({
            order: {
                id: 'ASC',
            },
            relations: ['redemptionCode', 'updatedByUser', 'customer', 'outlet'],
        });

        // Throw error if no records was found
        if (!redemptionRecords) {
            throw new RedemptionRecordNotFound('No redemption records was found!');
        }

        // Sort
        const personalRecords = [];
        await redemptionRecords.forEach((record) => {
            if (record.__updatedByUser__.username === user.username) {
                return personalRecords.push(record);
            }
        });

        const personalRecordsDTO = plainToClass(ShowRedemptionRecordDTO, personalRecords, { excludeExtraneousValues: true });

        // Conversion
        return { personal: personalRecordsDTO };
    }
}
