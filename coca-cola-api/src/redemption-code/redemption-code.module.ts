import { CoreModule } from './../core/core.module';
import { Module } from '@nestjs/common';
import { RedemptionCodeController } from './redemption-code.controller';
import { AuthModule } from '../auth/auth.module';

@Module({
    imports: [CoreModule, AuthModule],
    controllers: [RedemptionCodeController],
})
export class RedemptionCodeModule {}
