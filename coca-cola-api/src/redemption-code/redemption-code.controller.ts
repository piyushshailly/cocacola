import { UpdateRedemptionCodeDTO } from '../models/redemption-code/update-redemption-code.dto';
import { RedemptionCodeService } from '../core/services/redemption-code.service';
import { ShowRedemptionCodeDTO } from '../models/redemption-code/show-redemption-code.dto';
import { Controller, Get, Put, Param, Body, ValidationPipe, UseGuards, UseFilters } from '@nestjs/common';
import { User as UserDec } from '../common/decorators/user.decorator';
import { User } from '../data/entities/user.entity';
import { AuthGuard } from '@nestjs/passport';
import { JwtAuthGuard } from '../common/guards/jwt-auth.guard';
import { EntityNotFoundFilter } from '../common/filters/entity-not-found.filter';
import { BadRequestFilter } from '../common/filters/bad-request.filter';
import { Roles } from '../common/decorators/roles.decorator';
import { RolesIndividualGuard } from '../common/guards/roles-individual.guard';

@Controller('codes')
@UseFilters(EntityNotFoundFilter, BadRequestFilter)
@UseGuards(AuthGuard(), JwtAuthGuard, RolesIndividualGuard)
export class RedemptionCodeController {
    constructor(
        private readonly redemptionCodeService: RedemptionCodeService,
    ) { }

    @Get()
    @Roles('admin')
    async getAllCodes(): Promise<ShowRedemptionCodeDTO[]> {
        return await this.redemptionCodeService.getAllCodes();
    }

    @Get(':id')
    @Roles('admin', 'user')
    async getCodeById(
        @Param('id') id: string,
    ): Promise<ShowRedemptionCodeDTO> {
        return await this.redemptionCodeService.getCodeById(id);
    }

    @Put(':id')
    @Roles('admin', 'user')
    async updateRedemptionCode(
        @Param('id') id: string,
        @Body(new ValidationPipe({ whitelist: true, transform: true })) status: UpdateRedemptionCodeDTO,
        @UserDec() user: User,
    ): Promise<ShowRedemptionCodeDTO> {
        return await this.redemptionCodeService.updateRedemptionCode(id, status, user);
    }

    @Put(':id/report')
    @Roles('admin', 'user')
    async reportRedemptionCode(
        @Param('id') id: string,
        @UserDec() user: User,
    ): Promise<ShowRedemptionCodeDTO> {
        return await this.redemptionCodeService.reportRedemptionCode(id, user);
    }
}
