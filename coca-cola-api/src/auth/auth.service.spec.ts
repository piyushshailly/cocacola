import { Test, TestingModule } from '@nestjs/testing';
import { AuthService } from './auth.service';
import { LoginDTO } from '../models/user/login.dto';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from '../core/services/users.service';
import { BlacklistService } from './blacklist/blacklist.service';
import { UserBadRequest } from '../common/exceptions/user-bad-request';

describe('Auth Service', () => {
    let service: AuthService;

    const mockJwtService = {
        signAsync() { ''; },
    };
    const mockUsersService = {
        getUserByUsername() { ''; },
        validatePassword() { ''; },
        validateUser() { ''; },
    };
    const mockBlacklistService = {
        blacklist() { ''; },
    };

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [
                AuthService,
                {
                    provide: JwtService,
                    useValue: mockJwtService,
                },
                {
                    provide: UsersService,
                    useValue: mockUsersService,
                },
                {
                    provide: BlacklistService,
                    useValue: mockBlacklistService,
                },
            ],
        }).compile();

        service = module.get<AuthService>(AuthService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });

    it('login should return JWT token if correct login information is passed', async () => {
        const correctUserData: LoginDTO = {
            username: 'username',
            password: 'password',
        };
        const userEntity: any = {};
        const token = 'token';

        const validatePasswordSpy = jest
            .spyOn(mockUsersService, 'validatePassword')
            .mockImplementation(() => true);
        const getUserByUsernameSpy = jest
            .spyOn(mockUsersService, 'getUserByUsername')
            .mockImplementation(() => userEntity);
        const signAsyncSpy = jest
            .spyOn(mockJwtService, 'signAsync')
            .mockImplementation(() => token);

        const result = await service.login(correctUserData);

        expect(result).toBe('token');
        expect(validatePasswordSpy).toHaveBeenCalledTimes(1);
        expect(getUserByUsernameSpy).toHaveBeenCalledTimes(1);
        expect(signAsyncSpy).toHaveBeenCalledTimes(1);

        validatePasswordSpy.mockRestore();
        getUserByUsernameSpy.mockRestore();
        signAsyncSpy.mockRestore();
    });

    it('login should throw if incorrect login information is passed', async () => {
        try {
            const incorrectUserData: LoginDTO = {
                username: 'username',
                password: 'password',
            };
            const userEntity: any = {};
            const token = 'token';

            const validatePasswordSpy = jest
                .spyOn(mockUsersService, 'validatePassword')
                .mockImplementation(() => false);
            const getUserByUsernameSpy = jest
                .spyOn(mockUsersService, 'getUserByUsername')
                .mockImplementation(() => userEntity);
            const signAsyncSpy = jest
                .spyOn(mockJwtService, 'signAsync')
                .mockImplementation(() => token);

            await service.login(incorrectUserData);
            expect(validatePasswordSpy).toHaveBeenCalledTimes(1);
            expect(getUserByUsernameSpy).toHaveBeenCalledTimes(0);
            expect(signAsyncSpy).toHaveBeenCalledTimes(0);

            validatePasswordSpy.mockRestore();
            getUserByUsernameSpy.mockRestore();
            signAsyncSpy.mockRestore();
        } catch (err) {
            expect(err).toBeInstanceOf(UserBadRequest);
        }
    });

    it('logout should return the blacklisted JWT token', async () => {

        const token = 'token';

        const blacklistSpy = jest
            .spyOn(mockBlacklistService, 'blacklist')
            .mockImplementation(() => token);

        const result = await service.logout(token);

        expect(result).toEqual('token');
        expect(blacklistSpy).toHaveBeenCalledTimes(1);

        blacklistSpy.mockRestore();
    });

    it('validateUser should return UserDTO if payload is correct', async () => {

        const payload: any = {};
        const userDTO: any = {username: 'username'};

        const validateUserSpy = jest
            .spyOn(mockUsersService, 'validateUser')
            .mockImplementation(() => userDTO);

        const result = await service.validateUser(payload);

        expect(result).toEqual({username: 'username'});
        expect(validateUserSpy).toHaveBeenCalledTimes(1);

        validateUserSpy.mockRestore();
    });
});
