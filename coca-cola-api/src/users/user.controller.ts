import { EntityNotFoundFilter } from './../common/filters/entity-not-found.filter';
import { BadRequestFilter } from './../common/filters/bad-request.filter';
import { EditUserDTO } from './../models/user/edit-user.dto';
import { UserDTO } from '../models/user/user.dto';
import { Controller, Get, Post, Put, Delete, Param, Body, ValidationPipe, UseFilters, UseGuards } from '@nestjs/common';
import { UsersService } from 'src/core/services/users.service';
import { CreateUserDTO } from 'src/models/user/create-user.dto';
import { AuthGuard } from '@nestjs/passport';
import { JwtAuthGuard } from '../common/guards/jwt-auth.guard';
import { RolesGuard } from '../common/guards/roles.guard';
import { Roles } from '../common/decorators/roles.decorator';

@Roles('admin')
@UseFilters(BadRequestFilter, EntityNotFoundFilter)
@Controller('users')
@UseGuards(AuthGuard(), JwtAuthGuard, RolesGuard)
export class UserController {

    constructor( private readonly userService: UsersService ) {}

    @Get()
    async getAllUsers(): Promise<UserDTO[]> {
        return await this.userService.getAllUsers();
    }

    @Get(':id')
    async getUserById(@Param('id') id: string): Promise<UserDTO> {
        return await this.userService.getUserById(id);
    }

    @Post()
    async createUser(
        @Body(new ValidationPipe({whitelist: true, transform: true}))
        user: CreateUserDTO): Promise<UserDTO> {
        return await this.userService.createUser(user);
    }

    @Put(':id')
    async editUser(
        @Param('id') id: string,
        @Body(new ValidationPipe({whitelist: true, transform: true}))
        editedUser: EditUserDTO): Promise<UserDTO> {
        return await this.userService.editUser(id, editedUser);
    }

    @Delete(':id')
    async deleteUser(@Param('id') id: string): Promise<UserDTO> {
        return await this.userService.deleteUser(id);
    }
}
