import { RolesGuard } from './../common/guards/roles.guard';
import { EntityNotFoundFilter } from './../common/filters/entity-not-found.filter';
import { BadRequestFilter } from './../common/filters/bad-request.filter';
import { CustomerDTO } from './../models/customer/customer.dto';
import { CustomerService } from 'src/core/services/customer.service';
import { CreateCustomerDTO } from 'src/models/customer/create-customer.dto';
import { CreateCustomerResponseDTO } from 'src/models/customer/create-response.dto';
import { Body, Param, Controller, Post, Delete, Put, Get, ValidationPipe, UseFilters, UseGuards } from '@nestjs/common';
import { EditCustomerDTO } from 'src/models/customer/edit-customer.dto';
import { AuthGuard } from '@nestjs/passport';
import { JwtAuthGuard } from '../common/guards/jwt-auth.guard';
import { Roles } from '../common/decorators/roles.decorator';

@Roles('admin')
@UseFilters(BadRequestFilter, EntityNotFoundFilter)
@Controller('customers')
@UseGuards(AuthGuard(), JwtAuthGuard, RolesGuard)
export class CustomerController {
    constructor(private readonly customerService: CustomerService) {}

    @Get()
    async getAllCustomers(): Promise<CustomerDTO[]> {
        return await this.customerService.getAllCustomers();
    }

    @Get(':id')
    async getCustomerById(@Param('id') id: string): Promise<CustomerDTO> {
        return await this.customerService.getCustomerById(id);
    }

    @Post()
    async createCustomer(
        @Body(new ValidationPipe({whitelist: true, transform: true}))
        newCustomer: CreateCustomerDTO): Promise<CreateCustomerResponseDTO> {
        return await this.customerService.createCustomer(newCustomer);
    }

    @Put(':id')
    async editCustomer(
        @Param('id') id: string,
        @Body(new ValidationPipe({whitelist: true, transform: true}))
        content: EditCustomerDTO ): Promise<EditCustomerDTO> {
        return await this.customerService.editCustomer(id, content);
    }

    @Delete(':id')
    async deleteCustomer(@Param('id') id: string): Promise<CustomerDTO> {
        return await this.customerService.deleteCustomer(id);
    }
}
