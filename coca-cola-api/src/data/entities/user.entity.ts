import { RedemptionRecord } from './redemption-record.entity';
import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, ManyToOne, OneToMany, ManyToMany, JoinTable } from 'typeorm';
import { Outlet } from './outlet.entity';
import { Customer } from './customer.entity';
import { RedemptionCode } from './redemption-code.entity';
import { Roles } from './roles.entity';

@Entity('user')
export class User {

    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column('nvarchar', { unique: true })
    username: string;

    @Column()
    password: string;

    @Column('nvarchar')
    firstName: string;

    @Column('nvarchar')
    lastName: string;

    @ManyToOne(type => Customer, customer => customer.users)
    customer: Promise<Customer>;

    @ManyToOne(type => Outlet, outlet => outlet.users)
    outlet: Promise<Outlet>;

    @OneToMany(type => RedemptionCode, redemptionCodes => redemptionCodes.redeemedFrom)
    redemptionCodes: Promise<RedemptionCode[]>;

    @OneToMany(type => RedemptionRecord, redemptionRecord => redemptionRecord.updatedByUser)
    redeptionRecords: Promise<RedemptionRecord>;

    @CreateDateColumn()
    dateCreated: Date;

    @JoinTable()
    @ManyToMany(type => Roles, { eager: true })
    roles: Roles[];

    @Column({ default: false })
    isDeleted: boolean;
}
