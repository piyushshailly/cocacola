import { RedemptionCodeStatus } from './../../common/enums/redemption-code-status-type';
import { Customer } from './customer.entity';
import { User } from './user.entity';
import { PrimaryGeneratedColumn, Column, CreateDateColumn, UpdateDateColumn, Entity, ManyToOne, JoinColumn, OneToMany } from 'typeorm';
import { Outlet } from './outlet.entity';
import { RedemptionRecord } from './redemption-record.entity';

@Entity('redemption-code')
export class RedemptionCode {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column()
    code: string;

    @Column({default: RedemptionCodeStatus.valid})
    status: RedemptionCodeStatus;

    @UpdateDateColumn()
    redeemedAt: Date;

    @ManyToOne(type => Outlet, outlet => outlet.redemptionCodes, {eager: true})
    redeemedAtOutlet: Promise<Outlet>;

    @ManyToOne(type => User, user => user.redemptionCodes, {eager: true})
    redeemedFrom: Promise<User>;

    @ManyToOne(type => Customer, customer => customer.redemptionCodes, {eager: true})
    redeemedFromCustomer: Promise<Customer>;

    @OneToMany(type => RedemptionRecord, redemptionRecord => redemptionRecord.redemptionCode)
    @JoinColumn()
    redemptionRecord: Promise<RedemptionRecord[]>;

    @CreateDateColumn()
    dateCreated: Date;

    @Column({default: false})
    reported: boolean;
}
