import { IsString, MinLength, IsNotEmpty } from 'class-validator';

export class EditUserDTO {
    @IsString()
    username: string;

    @IsString()
    password: string;

    @IsString()
    @MinLength(2)
    firstName: string;

    @IsString()
    @MinLength(2)
    lastName: string;

}
