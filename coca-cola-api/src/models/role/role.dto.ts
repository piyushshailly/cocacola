import { Expose } from "class-transformer";

export class RoleDTO {
    @Expose()
    name: string;
}
