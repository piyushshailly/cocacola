import { Expose, Type } from 'class-transformer';
import { OutletDTO } from '../outlet/outlet.dto';

export class CustomerDTO {
    @Expose()
    id: string;

    @Expose()
    name: string;

    @Expose()
    dateCreated: Date;

    @Expose({ name: '__outlets__' })
    @Type(() => OutletDTO)
    outlets: OutletDTO[];

    // @Expose()
    // outlets: string[];

    // @Expose()
    // users: string[];
}
