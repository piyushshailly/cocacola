import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { RedemptionDataService } from '../core/services/redemptions-data.service';
import { Observable } from 'rxjs';
import { Redemption } from '../models/redemption';

@Injectable()
export class RedemptionsResolver implements Resolve<Observable<Redemption[]>> {

  constructor(private redemptionDataService: RedemptionDataService) {}

  resolve() {
    return this.redemptionDataService.getAllRedemptions();
  }
}
