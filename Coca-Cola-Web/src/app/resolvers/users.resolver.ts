import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { UserDataService } from '../core/services/user-data.service';
import { Observable } from 'rxjs';
import { User } from '../models/user';

@Injectable()
export class UsersResolver implements Resolve<Observable<User[]>> {

  constructor(private userDataService: UserDataService) {}

  resolve() {
    return this.userDataService.getAllUsers();
  }
}
