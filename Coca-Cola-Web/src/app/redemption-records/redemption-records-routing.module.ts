import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { RedemptionRecordsComponent } from './redemption-records.component';

const routes: Routes = [
  { path: '', component: RedemptionRecordsComponent, pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RedemptionRecordsRoutingModule { }
