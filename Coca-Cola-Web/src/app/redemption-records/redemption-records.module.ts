import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RedemptionRecordsComponent } from './redemption-records.component';
import { RedemptionRecordsRoutingModule } from './redemption-records-routing.module';
import { NgxDonutChartModule } from 'ngx-doughnut-chart';
import { RedemptionTableComponent } from './redemption-table/redemption-table.component';
import { SharedModule } from '../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ChartsModule } from 'ng2-charts';

@NgModule({
  declarations: [RedemptionRecordsComponent, RedemptionTableComponent],
  imports: [
    RedemptionRecordsRoutingModule,
    CommonModule,
    NgxDonutChartModule,
    SharedModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    ChartsModule
  ],
})
export class RedemptionRecordsModule { }
