import { Component, OnInit, ViewChildren, QueryList, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { Observable } from 'rxjs';
import { Redemption } from 'src/app/models/redemption';
import { RedemptionTableService } from './redemption-table.service';
import { NgbdSortableHeader, SortEvent } from '../../directives/sortable.directive';
import { Router } from '@angular/router';
import { Label, SingleDataSet } from 'ng2-charts';
import { ChartType } from 'chart.js';

@Component({
  selector: 'app-redemption-table',
  templateUrl: './redemption-table.component.html',
  styleUrls: ['./redemption-table.component.css']
})
export class RedemptionTableComponent implements OnInit, OnChanges {
  @Input()
  redemptions: Redemption[];
  redemptions$: Observable<Redemption[]>;
  char$: Observable<SingleDataSet>;
  total$: Observable<number>;
  sum = 0;
  public doughnutChartLabels: Label[] = ['Redeemed', 'Canceled', 'Reported'];
  public doughnutChartData: SingleDataSet = [
    0, 0, 0
  ];
  public doughnutChartType: ChartType = 'doughnut';

  @ViewChildren(NgbdSortableHeader) headers: QueryList<NgbdSortableHeader>;

  constructor(
    public service: RedemptionTableService,
    public router: Router,
  ) {
  }

  onSort({ column, direction, sub }: SortEvent) {
    // resetting other headers
    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = '';
      }
    });

    this.service.sortColumn = column;
    this.service.sortDirection = direction;
    this.service.sub = sub;
  }

  ngOnInit() {
    this.service.redemptions = this.redemptions;
    this.redemptions$ = this.service.redemptions$;
    this.total$ = this.service.total$;
    const redeemed = this.redemptions.filter((redemption: Redemption) => redemption.status === 'redeemed');
    const canceled = this.redemptions.filter((redemption: Redemption) => redemption.status === 'canceled');
    const reported = this.redemptions.filter((redemption: Redemption) => redemption.status === 'reported');
    this.doughnutChartData[0] = redeemed.length;
    this.doughnutChartData[1] = canceled.length;
    this.doughnutChartData[2] = reported.length;
    this.sum = +this.doughnutChartData[0].toString() + +this.doughnutChartData[1].toString();
    this.char$ = this.service.char$;
  }

  ngOnChanges(changes: SimpleChanges) {
    this.redemptions = changes.redemptions.currentValue;
    this.service.redemptions = this.redemptions;
    this.service.reset();
    this.redemptions$ = this.service.redemptions$;
    this.total$ = this.service.total$;
    this.char$ = this.service.char$;
  }
}
