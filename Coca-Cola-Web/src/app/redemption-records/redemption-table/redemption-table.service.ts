import { Injectable, PipeTransform } from '@angular/core';

import { BehaviorSubject, Observable, of, Subject } from 'rxjs';

import { DecimalPipe } from '@angular/common';
import { debounceTime, delay, switchMap, tap } from 'rxjs/operators';
import { Redemption } from 'src/app/models/redemption';
import { SortDirection } from '../../directives/sortable.directive';
import { SingleDataSet } from 'ng2-charts';

interface SearchResult {
    redemptions: Redemption[];
    total: number;
    doughnutChartData;
}

interface State {
    page: number;
    pageSize: number;
    searchTerm: string;
    sortColumn: string;
    sortDirection: SortDirection;
    sub;
}

function compare(v1, v2) {
    return v1 < v2 ? -1 : v1 > v2 ? 1 : 0;
}

function sort(redemptions: Redemption[], column: string, direction: string, sub: string): Redemption[] {
    if (direction === '') {
        return redemptions;
    } else {
        return [...redemptions].sort((a, b) => {
            if (sub) {
                const res1 = compare(a[column][sub], b[column][sub]);
                return direction === 'asc' ? res1 : -res1;
            }
            const res = compare(a[column], b[column]);
            return direction === 'asc' ? res : -res;
        });
    }
}

function matches(redemption: Redemption, term: string, pipe: PipeTransform) {
    return redemption.redemptionCode.code.toLowerCase().includes(term.toLowerCase())
        || redemption.updatedByUser.username.toLowerCase().includes(term.toLowerCase())
        || redemption.outlet.name.toLowerCase().includes(term.toLowerCase())
        || redemption.customer.name.toLowerCase().includes(term.toLowerCase())
        || redemption.status.toLowerCase().includes(term.toLowerCase())
        || redemption.dateCreated.toString().toLowerCase().includes(term.toLowerCase());
}

@Injectable({ providedIn: 'root' })
export class RedemptionTableService {
    // tslint:disable: variable-name
    private _loading$ = new BehaviorSubject<boolean>(true);
    private _search$ = new Subject<void>();
    private _redemptions$ = new BehaviorSubject<Redemption[]>([]);
    private _char$ = new BehaviorSubject<SingleDataSet>([]);
    private _total$ = new BehaviorSubject<number>(0);
    private _redemptions: Redemption[];

    private _state: State = {
        page: 1,
        pageSize: 10,
        searchTerm: '',
        sortColumn: '',
        sortDirection: '',
        sub: ''
    };

    constructor(private pipe: DecimalPipe) {
        this._search$.pipe(
            tap(() => this._loading$.next(true)),
            debounceTime(200),
            switchMap(() => this._search()),
            delay(200),
            tap(() => this._loading$.next(false))
        ).subscribe(result => {
            this._redemptions$.next(result.redemptions);
            this._char$.next(result.doughnutChartData)
            this._total$.next(result.total);
        });

        this._search$.next();
    }

    get redemptions$() { return this._redemptions$.asObservable(); }

    get char$() { return this._char$.asObservable(); }

    get total$() { return this._total$.asObservable(); }

    get loading$() { return this._loading$.asObservable(); }

    get page() { return this._state.page; }
    set page(page: number) { this._set({ page }); }

    get pageSize() { return this._state.pageSize; }
    set pageSize(pageSize: number) { this._set({ pageSize }); }

    get searchTerm() { return this._state.searchTerm; }
    set searchTerm(searchTerm: string) { this._set({ searchTerm }); }

    set sortColumn(sortColumn: string) { this._set({ sortColumn }); }

    set sortDirection(sortDirection: SortDirection) { this._set({ sortDirection }); }

    set sub(sub: string) { this._set({ sub }); }

    set redemptions(redemptions: Redemption[]) { this._redemptions = redemptions; }

    reset() {
        this._search$.pipe(
            tap(() => this._loading$.next(true)),
            debounceTime(200),
            switchMap(() => this._search()),
            delay(200),
            tap(() => this._loading$.next(false))
        ).subscribe(result => {
            this._redemptions$.next(result.redemptions);
            this._total$.next(result.total);
        }, err => { });

        this._search$.next();
    }

    private _set(patch: Partial<State>) {
        Object.assign(this._state, patch);
        this._search$.next();
    }

    private _search(): Observable<SearchResult> {
        const { sortColumn, sortDirection, pageSize, page, searchTerm, sub } = this._state;

        // 1. sort
        let redemptions = sort(this._redemptions, sortColumn, sortDirection, sub);

        // 2. filter
        redemptions = redemptions.filter(redemption => matches(redemption, searchTerm, this.pipe));
        const total = redemptions.length;

        // 3. paginate
        redemptions = redemptions.slice((page - 1) * pageSize, (page - 1) * pageSize + pageSize);

        const redeemed = redemptions.filter((redemption: Redemption) => redemption.status === 'redeemed');
        const canceled = redemptions.filter((redemption: Redemption) => redemption.status === 'canceled');
        const reported = redemptions.filter((redemption: Redemption) => redemption.status === 'reported');
        const doughnutChartData = [];
        doughnutChartData[0] = redeemed.length;
        doughnutChartData[1] = canceled.length;
        doughnutChartData[2] = reported.length;

        return of({ redemptions, total, doughnutChartData });
    }
}
