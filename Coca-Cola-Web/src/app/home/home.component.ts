import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  images = [
    '../../assets/images/0.png',
    '../../assets/images/1.png',
    '../../assets/images/2.png',
    '../../assets/images/3.png',
  ];
  constructor() { }

  ngOnInit() {
  }

}
