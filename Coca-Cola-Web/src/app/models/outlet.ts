import { Customer } from './customer';
import { User } from './user';

export interface Outlet {
    id: string;
    name: string;
    address: string;
    customer: Customer;
    users: User[];
    dateCreated: string;
}
