export interface UserEdit {

    username: string;

    password: string;

    firstName: string;

    lastName: string;

}
