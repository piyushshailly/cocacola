import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotFoundComponent } from './not-found/not-found.component';
import { ErrorComponent } from './error/error.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgbdSortableHeader } from '../directives/sortable.directive';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    NotFoundComponent,
    ErrorComponent,
    NgbdSortableHeader,
  ],
  imports: [
    CommonModule,
    NgbModule,
    ReactiveFormsModule,
  ],
  exports: [
    NotFoundComponent,
    ErrorComponent,
    NgbModule,
    NgbdSortableHeader,
    ReactiveFormsModule,
  ]
})
export class SharedModule { }
