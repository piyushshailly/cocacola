import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule, DecimalPipe } from '@angular/common';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { StorageService } from './services/storage.service';
import { AuthGuardService } from './services/auth-guard.service';
import { AuthService } from './services/auth.service';
import { UserDataService } from './services/user-data.service';
import { AdminGuardService } from './services/admin-guard.service';
import { RedemptionDataService } from './services/redemptions-data.service';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    HttpClientModule
  ],
  providers: [
    HttpClient,
    StorageService,
    AuthGuardService,
    AuthService,
    DecimalPipe,
    UserDataService,
    AdminGuardService,
    RedemptionDataService,
  ],
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parent: CoreModule) {
    if (parent) {
      throw new Error('Core module is already provided!');
    }
  }
}
