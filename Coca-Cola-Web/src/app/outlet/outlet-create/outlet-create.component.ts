import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Outlet } from '../../models/outlet';
import { OutletCreate } from '../../models/outlet-create';
import { Customer } from '../../models/customer';

@Component({
  selector: 'app-outlet-create',
  templateUrl: 'outlet-create.component.html',
  styleUrls: ['outlet-create.component.css']
})
export class OutletCreateComponent implements OnInit {
  @Input()
  customers: Customer[];
  @Output()
  outletCreatedEvent = new EventEmitter<any>();
  registerForm: FormGroup;
  submitted = false;
  singleSelect: Outlet;
  config = {
    displayKey: 'name',
    search: true,
    limitTo: 30,
  };
  options: Customer[] = [];

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.options = this.customers;
    this.registerForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(2)]],
      address: ['', [Validators.required, Validators.minLength(2)]]
    });
  }

  get f() { return this.registerForm.controls; }

  onSubmit() {
    this.submitted = true;

    if (this.registerForm.invalid) {
      return;
    }

    const outlet: OutletCreate = {
      name: this.registerForm.value.name,
      address: this.registerForm.value.address,
    };
    const customerId = this.singleSelect.id;

    this.outletCreatedEvent.emit({ outlet, customerId });
  }

}
