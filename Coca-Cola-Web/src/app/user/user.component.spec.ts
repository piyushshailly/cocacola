import { TestBed, async } from '@angular/core/testing';
import { UserComponent } from './user.component';
import { ActivatedRoute } from '@angular/router';
import { CommonModule, DecimalPipe } from '@angular/common';
import { of } from 'rxjs';
import { UserTableComponent } from './user-table/user-table.component';
import { UserCreateComponent } from './user-create/user-create.component';
import { UserEditComponent } from './user-edit/user-edit.component';
import { UserRoutingModule } from './user-routing.module';
import { SharedModule } from '../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SelectDropDownModule } from 'ngx-select-dropdown';
import { UserDataService } from '../core/services/user-data.service';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { asyncError } from '../testing/async-observable-helpers';
import { UserService } from './user.service';
import { ToastrService } from 'ngx-toastr';


describe('UserComponent', () => {
    let fixture;
    let users: any = [
        {
            user: 'user1',
            id: 1,
        },
        {
            user: 'user2',
            id: 2,
        }
    ];
    const userDataService = jasmine.createSpyObj('UsersDataService', ['deleteUser', 'createUser', 'editUser']);
    const userService = jasmine.createSpyObj('UsersService', ['getData']);
    const toastrService = jasmine.createSpyObj('ToastrService', ['success']);
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                UserComponent,
                UserTableComponent,
                UserCreateComponent,
                UserEditComponent,
            ],
            imports: [
                CommonModule,
                UserRoutingModule,
                SharedModule,
                FormsModule,
                ReactiveFormsModule,
                NgbModule,
                SelectDropDownModule,
            ],
            providers: [
                {
                    provide: ActivatedRoute,
                    useValue: { data: of({ users, outletsResolve: 'outletsResolve' }) },
                },
                {
                    provide: UserDataService,
                    useValue: userDataService
                },
                {
                    provide: UserService,
                    useValue: userService
                },
                {
                    provide: ToastrService,
                    useValue: toastrService
                },
                DecimalPipe

            ],
            schemas: [NO_ERRORS_SCHEMA]
        });
    }));

    afterEach(() => {
        if (fixture.nativeElement && 'remove' in fixture.nativeElement) {
            (fixture.nativeElement as HTMLElement).remove();
        }
    });

    it('should create the component', () => {
        fixture = TestBed.createComponent(UserComponent);
        const component = fixture.debugElement.componentInstance;
        expect(component).toBeTruthy();
    });

    describe('OnInit', () => {
        it('should initialize with the correct users data', async () => {

            fixture = TestBed.createComponent(UserComponent);
            const component = fixture.debugElement.componentInstance;
            userService.getData.and.returnValue({ users, outletsResolve: 'outletsResolve' });

            await fixture.detectChanges();
            expect(component.users).toEqual(users);
            expect(component.outletsResolve).toEqual('outletsResolve');
        });
    });

    describe('deleteUser', () => {
        it('should delete the user and update the array', async () => {
            const user1 = {
                user: 'user1',
                id: 1,
            };
            const user2 = {
                user: 'user2',
                id: 2,
            };
            fixture = TestBed.createComponent(UserComponent);
            const component = fixture.debugElement.componentInstance;
            userDataService.deleteUser.and.returnValue(of(user2));
            userService.getData.and.returnValue({ users, outletsResolve: 'outletsResolve' });
            await fixture.detectChanges();
            component.deleteUser(1);
            expect(component.users).toEqual([user2]);
        });

        it('should not throw when getting http error code', async () => {
            const user1 = {
                user: 'user1',
                id: 1,
            };
            const user2 = {
                user: 'user2',
                id: 2,
            };
            fixture = TestBed.createComponent(UserComponent);
            const component = fixture.debugElement.componentInstance;
            userDataService.deleteUser.and.returnValue(asyncError(''));
            component.deleteUser(1);
            await fixture.detectChanges();
            expect(component.users).toEqual(users);
        });
    });

    describe('createUser', () => {
        it('should create the user and update the array', async () => {
            const user3 = {
                user: 'user3',
                id: 3,
            };
            fixture = TestBed.createComponent(UserComponent);
            const component = fixture.debugElement.componentInstance;
            userDataService.createUser.and.returnValue(of(user3));
            await fixture.detectChanges();
            component.createUser(user3);
            expect(component.users).toEqual([...users, user3]);
        });
    });

    describe('editUser', () => {
        it('should create the user and update the array', async () => {
            const user2 = {
                user: 'user3',
                id: 2,
            };
            const newData = { user: user2, id: 2 };
            users = [
                {
                    user: 'user1',
                    id: 1,
                },
                {
                    user: 'user2',
                    id: 2,
                }
            ];
            const updatedUsers = [
                {
                    user: 'user1',
                    id: 1,
                },
                {
                    user: 'user3',
                    id: 2,
                }
            ];
            fixture = TestBed.createComponent(UserComponent);
            const component = fixture.debugElement.componentInstance;
            userDataService.editUser.and.returnValue(of(user2));
            await fixture.detectChanges();
            component.editUser(newData);
            expect(component.users).toEqual(updatedUsers);
        });

        it('should not throw when getting http error code', async () => {
            fixture = TestBed.createComponent(UserComponent);
            const component = fixture.debugElement.componentInstance;
            userDataService.editUser.and.returnValue(asyncError(''));
            component.editUser(1);
            await fixture.detectChanges();
            expect(component.users).toEqual(users);
        });
    });
});
