import { Injectable, PipeTransform } from '@angular/core';

import { BehaviorSubject, Observable, of, Subject } from 'rxjs';

import { DecimalPipe } from '@angular/common';
import { debounceTime, delay, switchMap, tap } from 'rxjs/operators';
import { User } from 'src/app/models/user';
import { SortDirection } from '../../directives/sortable.directive';

interface SearchResult {
    users: User[];
    total: number;
}

interface State {
    page: number;
    pageSize: number;
    searchTerm: string;
    sortColumn: string;
    sortDirection: SortDirection;
    sub: string;
}

function compare(v1, v2) {
    return v1 < v2 ? -1 : v1 > v2 ? 1 : 0;
}

function sort(users: User[], column: string, direction: string, sub: string): User[] {
    if (direction === '') {
        return users;
    } else {
        return [...users].sort((a, b) => {
            if (sub) {
                const res1 = compare(a[column][sub], b[column][sub]);
                return direction === 'asc' ? res1 : -res1;
            }
            const res = compare(a[column], b[column]);
            return direction === 'asc' ? res : -res;
        });
    }
}

function matches(user: User, term: string, pipe: PipeTransform) {
    return user.username.toLowerCase().includes(term.toLowerCase())
        || user.id.toLowerCase().includes(term.toLowerCase())
        || user.firstName.toLowerCase().includes(term.toLowerCase())
        || user.lastName.toLowerCase().includes(term.toLowerCase())
        || user.outlet.name.toLowerCase().includes(term.toLowerCase())
        || user.customer.name.toLowerCase().includes(term.toLowerCase());
}

@Injectable({ providedIn: 'root' })
export class UserTableService {
    // tslint:disable: variable-name
    private _loading$ = new BehaviorSubject<boolean>(true);
    private _search$ = new Subject<void>();
    private _users$ = new BehaviorSubject<User[]>([]);
    private _total$ = new BehaviorSubject<number>(0);
    private _users: User[];

    private _state: State = {
        page: 1,
        pageSize: 10,
        searchTerm: '',
        sortColumn: '',
        sortDirection: '',
        sub: ''
    };

    constructor(private pipe: DecimalPipe) {
        this._search$.pipe(
            tap(() => this._loading$.next(true)),
            debounceTime(200),
            switchMap(() => this._search()),
            delay(200),
            tap(() => this._loading$.next(false))
        ).subscribe(result => {
            this._users$.next(result.users);
            this._total$.next(result.total);
        }, err => {});

        this._search$.next();
    }

    get users$() { return this._users$.asObservable(); }

    get total$() { return this._total$.asObservable(); }

    get loading$() { return this._loading$.asObservable(); }

    get page() { return this._state.page; }
    set page(page: number) { this._set({ page }); }

    get pageSize() { return this._state.pageSize; }
    set pageSize(pageSize: number) { this._set({ pageSize }); }

    get searchTerm() { return this._state.searchTerm; }
    set searchTerm(searchTerm: string) { this._set({ searchTerm }); }

    set sortColumn(sortColumn: string) { this._set({ sortColumn }); }

    set sortDirection(sortDirection: SortDirection) { this._set({ sortDirection }); }
    get sortDirection() { return this._state.sortDirection; }

    set sub(sub: string) { this._set({ sub }); }

    set users(users: User[]) { this._users = users; }

    reset() {
        this._search$.pipe(
            tap(() => this._loading$.next(true)),
            debounceTime(200),
            switchMap(() => this._search()),
            delay(200),
            tap(() => this._loading$.next(false))
        ).subscribe(result => {
            this._users$.next(result.users);
            this._total$.next(result.total);
        }, err => {});

        this._search$.next();
    }

    private _set(patch: Partial<State>) {
        Object.assign(this._state, patch);
        this._search$.next();
    }

    private _search(): Observable<SearchResult> {
        const { sortColumn, sortDirection, pageSize, page, searchTerm, sub } = this._state;

        // 1. sort
        let users = sort(this._users, sortColumn, sortDirection, sub);

        // 2. filter
        users = users.filter(user => matches(user, searchTerm, this.pipe));
        const total = users.length;

        // 3. paginate
        users = users.slice((page - 1) * pageSize, (page - 1) * pageSize + pageSize);
        return of({ users, total });
    }
}
