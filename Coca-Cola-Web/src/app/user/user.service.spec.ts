import { TestBed } from '@angular/core/testing';
import { UserService } from './user.service';

describe('UserService', () => {

    beforeEach(() => TestBed.configureTestingModule({
    }));

    it('should be created', () => {
        const service: UserService = TestBed.get(UserService);

        expect(service).toBeTruthy();
    });

    describe('getData', () => {
        it('should proper user data when loaded from "/users"', () => {
            const users = {
                users: ['users'],
                outletsResolve: ['outlets'],
            };
            const route = {
                snapshot: {
                    params: {
                        id: '',
                    },
                },
            };

            const service: UserService = TestBed.get(UserService);
            const result = service.getData(users, route);

            expect(result).toEqual({users: users.users, outletsResolve: users.outletsResolve});
        });

        it('should proper user data when loaded from "/outlets/:id/users"', () => {
            const users2 = {
                outletsResolve: ['outlets'],
                outlets: [{id: 1, users: [{}], customer: ''}],
            };
            const users = { outlet: users2.outlets[0], customer: '' };
            users.outlet.users = users.outlet.users;
            const route = {
                snapshot: {
                    params: {
                        id: 1,
                    },
                },
            };

            const service: UserService = TestBed.get(UserService);
            const result = service.getData(users2, route);

            expect(result).toEqual({users: [users], outletsResolve: users2.outlets});
        });
    });
});
