import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { MustMatch } from '../../validators/must-match.validator';
import { Outlet } from '../../models/outlet';
import { CustomValidators } from '../../validators/register-validator';
import { UserEdit } from '../../models/user-edit';
import { User } from 'src/app/models/user';

@Component({
  selector: 'app-user-edit',
  templateUrl: 'user-edit.component.html',
  styleUrls: ['user-edit.component.css']
})
export class UserEditComponent implements OnInit {
  @Input()
  userData: User;
  @Output()
  userEditedEvent = new EventEmitter<any>();
  @Output()
  cancelEdit = new EventEmitter();
  registerForm: FormGroup;
  submitted = false;
  id: string;

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.id = this.userData.id;
    this.registerForm = this.formBuilder.group({
      firstName: [this.userData.firstName, [Validators.required, Validators.minLength(2)]],
      lastName: [this.userData.lastName, [Validators.required, Validators.minLength(2)]],
      username: [this.userData.username, [Validators.required, Validators.minLength(2)]],
      password: ['', [Validators.required, Validators.minLength(6),
        CustomValidators.patternValidator(/\d/, {
        hasNumber: true
      }),
      CustomValidators.patternValidator(/[A-Z]/, {
        hasCapitalCase: true
      }),
      CustomValidators.patternValidator(/[a-z]/, {
        hasSmallCase: true
      }),
      CustomValidators.patternValidator(
        /[ !@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/,
        {
          hasSpecialCharacters: true
        }
      ),]],
      confirmPassword: ['', [Validators.required]]
    }, {
        validator: MustMatch('password', 'confirmPassword')
      });
  }

  get f() { return this.registerForm.controls; }

  onSubmit() {
    this.submitted = true;

    if (this.registerForm.invalid) {
      return;
    }

    const user: UserEdit = {
      username: this.registerForm.value.username,
      firstName: this.registerForm.value.firstName,
      lastName: this.registerForm.value.lastName,
      password: this.registerForm.value.password,
    };

    this.userEditedEvent.emit({user, id: this.id});
  }

  cancelEditing() {
    this.cancelEdit.emit();
  }

}
